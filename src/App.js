import {useState} from 'react';
import './App.css';

function App() {
  const [toDos, setTodos]=useState([])
  const [toDo, setTodo]=useState('')

  const deleteTask =(index)=>{
    var message =  window.confirm("Do you want to delete the task?");
    if(message){
      const test = [...toDos];
      test.splice(index,1);
      setTodos(test)
    }
    else{
      console.log("Dont Delete");
    }
  }
  return (
    <div className="app">
      <div className="mainHeading">
        <h1>ToDo List</h1>
      </div>
      <div className="subHeading">
        <br />
        <h2>Whoop, it's Wednesday 🌝 ☕ </h2>
      </div>
      <div className="input">
        <input value={toDo} onChange={(e)=>setTodo(e.target.value)} type="text" placeholder="🖊️ Add item..." />
        <i onClick={()=>setTodos([...toDos, {id:Date.now(), text: toDo, status:false}])} className="fas fa-plus"></i>
      </div>
      <div className="todos">
        {toDos.map((obj, index)=>{
          return(
            <div key={index} className="todo">
              <div className="left">
                <input onChange={(e)=>{
                  console.log(e.target.value)
                  console.log(obj)
                  setTodos(toDos.filter(obj2 => {
                    if (obj2.id===obj.id){
                      obj2.status = e.target.checked
                      console.log(obj2)
                    }
                    return obj2
                  }))
                }} value={obj.status} type="checkbox" name="" id="" />
                <p>{obj.text}</p>
              </div>
              <div className="right">
                <i onClick={()=> deleteTask(index)} className="fas fa-times"></i>
              </div>
            </div>
          )
        })}
        {
          toDos.map((obj, index)=>{
            if(obj.status){
              return (<h1 key={index}>{obj.text}</h1>)
            }return null
          })
        }
        
      </div>
    </div>
  );
}

export default App;
